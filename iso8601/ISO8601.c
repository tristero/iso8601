//
//  ISO8601.c
//  xJSON
//
//  Created by Jeffrey Laing on 10/09/12.
//  Copyright (c) 2012 Jeffrey Laing. All rights reserved.
//

#include "ISO8601.h"
#include <ctype.h>

// helper macro to truncate expressions to integers
#define	INT(i) ((int)(i))

// This is based on the 2004 spec found here:
// http://dotat.at/tmp/ISO_8601-2004_E.pdf
//
// Note that RFC3339 is much easier to parse.
//
// We need to cope with all of the following variants
//
// isodate::=	[<date>][[T]<time>]
//
// hyphen ::=	'-'
// colon  ::=	':'
// decimal ::=	{ ',' | '.' }
// sign ::=		{ '+' | '-' }
//
// date::=
// yyyy-mm-dd			; 4.1.2.2 (complete)
// yyyymmdd				;
// yyyy-mm				; 4.1.2.3 (reduced accuracy)
// yyyy					;
// yy					;
//
// yyyynnn				; 4.1.3.2 (complete)
// yyyy-nnn				;
//
// yyyyWwwn				; 4.1.4.2 (complete)
// yyyy-Www-n			;
// yyyyWww				; 4.1.4.3 (reduced accuracy)
// yyy-Www				;
//
// time::=
// hh:mm:ss				;4.2.2.2 (complete)
// hhmmss				;
// hhmm					;4.2.2.3 (reduced accuracy)
// hh:mm				;
// hh					;
//
// hhmmss<decimal>n+	;4.2.2.4 (decimal fraction)
// hh:mm:ss<decimal>n+	;
// hhmm<decimal>n+		;
// hh:mm<decimal>n+		;
// hh<decimal>n+		;
//
// <time>Z				; 4.2.4
// <time><sign>hh:mm	; 4.2.5
// <time><sign>hhmm		;
// <time><sign>hh		;
//
//
// At 2000, we also allowed truncated representation which fill in holes based on "current date" mostly.
//
// yymmdd
// yy-mm-dd
// -yymm
// -yy-mm
// -yy
// --mmdd
// --mm-dd
// ---dd
//
// yyddd
// yy-ddd
// -ddd
//
// yyWwwd
// yy-Www-d
// yy-Www
// YWwwD
// -y=Ww=D		??? implied decade???
// -yWww		???
// -y-Www		???
// -Wwwd
// -Www-d
// -Www
// -W-d

// slurp up the next set of characters, advancing *p and leaving
// it pointing to the first non-digit character we find.  Return
// the number of digits we think we parsed over and store the numeric
// value of that sequence in *valsofar
static
int slurp(
	const char **p,
	int32_t *valsofar )
{
	const char *s = *p;
	int len;
	int32_t value;
	value = 0;
	len = 0;
	while (*s) {
		if (!isdigit(*s)) break;
		value *= 10;
		value += (*s - '0');
		s++;
		len++;
	}
	*p = s;
	*valsofar = value;
	return len;
}

// slurp up the next set of characters which is assumed to be the
// fractional part of a time.  *p is assumed to be the decimal seperator.
static
double slurpf(
	const char **p )
{
	const char *s = *p;
	double value;
	double div = 10;
	value = 0;
	while (*++s && div!=0) {
		if (!isdigit(*s)) break;
		value += (*s-'0') / div;
		div /= 10;
	}
	*p = s;
	return value;
}

// update the tm structure to hold the first day (Monday)
// of week 1 in the nominated year.  note that this might
// result in the previous year, since week 1 is defined as
// starting on the Monday in the same week as Jan 4, where
// weeks run Monday to Sunday. For example, 2008-W01 actually
// starts on 2007/31/12 since 2008/1/1 is a Tuesday.
static void firstdayofweek1(struct tm *tm)
{
	// Using Zellers standard congruence, we would do the following:
	//	int d = 4;				// 4th
	//	int m = 13;			// January
	//	int y = tm.tm_year-1;	// Adjust for January
	//	int h = (d + (int)((m+1)*26/10) + y + ((int)(y/4)) + 6*((int)(y/100)) + (int)(y/400)) % 7;
	// which gives h = {sat=0, sun=1, mon=2, tue=3, wed=4, thu=5, fri=6,}
	//
	// For our math, life would be easier if we had:
	// h = {mon=0, tue=1, wed=2, thu=3, fri=4, sat=5, sun=6}
	// which we can achieve by subtracting 2 mod 7.
	// This means we have two discrete ranges of adjustments to
	// make, rather than three
	//
	// We can shortcut the day/month stuff since that's invariant
	// d + ((m+1)*26/10) - 2 => 4 + (14*26/10) - 2 = 38 => 38%7 = 3
	//
	int y = tm->tm_year+1900-1;
	int h = ( 3 + y + INT(y/4) + 6*INT(y/100) + INT(y/400) ) % 7;

	// h = {mon=0, tue=1, wed=2, thu=3, fri=4, sat=5, sun=6}
	// if Jan 4 is > 0, then we need to step backward by (h-1) days so that
	// we are on the first Monday.
	//		 0	 1	 2	 3	 4	 5	 6
	// J4=	MON	TUE	WED	THU	FRI SAT SUN
	// MON=	J4	J3	J2	J1  D31 D30 D29
	//
	// so, only h>3 can take us into the previous year
	//
	if (h>3) {
		tm->tm_mon = 11;	tm->tm_mday = 35-h;		tm->tm_year--;
	} else {
		tm->tm_mon = 0;		tm->tm_mday = 4-h;
	}
}

// add a specific number of days the date we have already
// stored.
static void adddays(struct tm *tm, int n)
{
	static int mlen[12] = {
	//	J	F	M	A	M	J	J	A	S	O	N	D
		31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
	};

	// remember, leap year calcs only work on the "full" year#
	tm->tm_year += 1900;

	// just blindly add - note, we don't work for subtraction
	tm->tm_mday += n;
	while (1) {
		// check if the #days is > max for this month
		int ml = mlen[tm->tm_mon];
		// deal with feb29
		if (1 == tm->tm_mon) {
			if ((tm->tm_year%4)==0) ml++;
			if ((tm->tm_year%100)==0) ml--;
			if ((tm->tm_year%400)==0) ml++;
		}
		if (tm->tm_mday <= ml) break;
		tm->tm_mday -= ml;
		if (++tm->tm_mon > 11) {
			tm->tm_year++;
			tm->tm_mon = 0;
		}
	}
	tm->tm_year -= 1900;
}

static void
setfromdoy(
	struct tm *tm,				// year is already populated
	int doy )
{
	tm->tm_mon = 0;
	tm->tm_mday = 1;
	if (doy>1) adddays(tm,doy-1);
}

static void
setfromwnum(
	struct tm *tm,				// year is already populated
	int wnum )
{
	firstdayofweek1(tm);
	if (wnum>1) adddays(tm, (wnum-1)*7);
}

// parse a date
const char *Parse8601(
	const char *utf8,
	time_t *result )
{
	struct tm tm;
	const char *p, *q;
	int32_t value;
	double frac;
	int32_t tmoff;
	
	// we initialise from localtime so that we have access to
	// the current timezone information.
	time_t now = time(NULL);
	tm = *localtime(&now);

	int century = (tm.tm_year+1900)/100;

	// point at the first character in the input
	p = utf8;
	while (isspace(*p)) p++;

#pragma mark State 0

	// collect up digits till we find a non-digit, then look at the count,
	// and perhaps the non-digit
	switch (slurp(&p, &value)) {
	case 0:
		// no date - defaults to 'now'
		if (*p == 'T') goto parse_time;
		break;

	case 2:
		switch (*p) {
			// yy<*>
			// yy<*>T...
		case '\000':
		case 'T': case 't':
			// according to 8601-2004:4.1.2.3, YY represents a CENTURY, not a YEAR.
			// and it counts as expanded, not truncated
			// This is the only case where YY doesn't represent YEAR within CENTURY
			tm.tm_year = (value-19)*100;
			tm.tm_mon = 0;
			tm.tm_mday = 1;
			goto parse_time;

		case '-':
			tm.tm_year = (100*INT(tm.tm_year/100))+value;
			if (*++p == 'W') {
				// yy-<*>Www-d
				// yy-<*>Www
				p++;
				goto state_W_;
			} else if (isdigit(*p)) {
				// yy-<*>mm-dd
				// yy-<*>ddd
				goto state_yy_;
			}
			break;

		case 'W':
			// yyWwwd
			tm.tm_year = (century-19)*100+value;
			p++;
			goto state_W_;
		}
		break;

	case 4:
		tm.tm_year = value - 1900;
		switch (*p) {
			// yyyy<*>
		case '\000':
		case 'T': case 't':
			tm.tm_mon = 0;
			tm.tm_mday = 1;
			goto parse_time;

			// yyyy<*>-mm-dd
			// yyyy<*>-mm
			// yyyy<*>-nnn
			// yyyy<*>-Www-n
			// yyyy<*>-Www
		case '-':
			p++;
			if (*p == 'W') {
				p++;
				goto state_W_;
			}
			goto state_yyyy_;

			// yyyy<*>Wwwn
			// yyyy<*>Www
		case 'W':
			p++;
			goto state_W_;
		}
		break;

	case 5:
		// yyddd - missing from 2004, found in 8601:2000:5.2.2.2
		// counts as truncated
		tm.tm_year = (century-19)*100+(value/1000);
		setfromdoy(&tm, value % 1000);
		goto parse_time;

	case 6:
		switch (*p) {
			// yymmdd<*>
		case '\000':
		case 'T': case 't':
			// yy = two digit year in *current* century
			tm.tm_year = (century-19)*100+(value/10000);
			tm.tm_mon = ((value/100) % 100) - 1;
			tm.tm_mday = (value%100);
			goto parse_time;
		}
		break;

	case 7:
		switch (*p) {
			// yyyynnn<*>				; -> time
		case '\000': case 'T':
			tm.tm_year = (value/1000) - 1900;
			setfromdoy(&tm, value%1000);
			goto parse_time;
		}
		break;

	case 8:
		switch (*p) {
			// yyyymmdd<*>				; -> time
		case '\000':
		case 'T': case 't':
			tm.tm_year = (value/10000) - 1900;
			tm.tm_mon = ((value/100) % 100) - 1;
			tm.tm_mday = (value%100);
			goto parse_time;
		}
		break;
	}

	// getting to here is a fail
	return p;

#pragma mark State 1

// here, we have successfully parsed a year followed by a hyphen
// yyyy-<*>mm-dd
// yyyy-<*>mm
// yyyy-<*>nnn
state_yyyy_:
// yy<*>-mm-dd
// yy<*>-ddd
state_yy_:

	switch (slurp(&p, &value)) {
	case 2:
		tm.tm_mon = value-1;
		switch (*p) {
			// yyyy-mm<*>
		case '\000': case 'T':
			tm.tm_mday = 1;
			goto parse_time;

			// yyyy-mm<*>-dd
		case '-':
			q = ++p;			// remember where this one started
			if (slurp(&p,&value)==2) {
				// yyyy-mm-dd<*>
				tm.tm_mday = value;
				goto parse_time;
			}
			return q;			// on failures, back up
		}
		break;

	case 3:
		// yyyy-nnn<*>
		setfromdoy(&tm, value);
		goto parse_time;
	}
	return p;

#pragma mark State 2

// here, we have parsed off the year number and now expect to see
// a two digit week# followed by a single digit day number
//
// yy-W<*>ww
// yyyy-W<*>ww
// yyyyW<*>ww
// yy-W<*>ww-d
// yyyy-W<*>ww-d
// yyW<*>wwd
// yyyyW<*>wwd
//
state_W_:
	switch (slurp(&p, &value)) {
	case 2:
		setfromwnum(&tm,value);
		switch (*p) {
			// yy-Www<*>
			// yyyy-Www<*>
			// yyyyWww<*>
		case '\000': case 'T':
			goto parse_time;

			// yy-Www<*>-d
			// yyyy-Www<*>-d
		case '-':
			q = ++p;			// remember where this one started
			if (slurp(&p,&value)==1) {
				// yy-Www-d<*>
				// yyyy-Www-d<*>
				if (value>1) adddays(&tm,value-1);
				goto parse_time;
			}
			p = q;			// on failures, back up
		}
		break;

	case 3:
		// yyWwwd<*>
		// yyyyWwwd<*>
		setfromwnum(&tm,value/10);
		adddays(&tm,(value%10)-1);
		goto parse_time;
	}
	return p;

#pragma mark State 3

parse_time:
	// TODO - validate date ranges so far...

	// silently swallow 'T'
	if (*p == 'T') {
		p++;
	}

	// initialise time to 00:00:00
	tm.tm_hour = 0;
	tm.tm_min = 0;
	tm.tm_sec = 0;

	// no time, consider that a success - however, since there is no
	// timezone either, we call it midnight, GMT.
	if (!*p) {
		*result = timegm(&tm);
		return 0;
	}

	switch (slurp(&p, &value)) {

		// hh<*>:mm:ss[,s]
		// hh<*>:mm[,m]
		// hh<*>[,h]
	case 2:
		tm.tm_hour = value;
		switch (*p) {
		default:
			goto parse_timezone;

		// hh<*>:mm:ss[,s]
		// hh<*>:mm[,m]
		case ':':
			p++;
			goto parse_mm;

		case '.': case ',':
			frac = slurpf(&p);
			if (frac) {
				// 1H = 3600S
				double mmss = frac*3600;
				tm.tm_min = INT(mmss/60);
				mmss -= tm.tm_min*60;
				tm.tm_sec = INT(mmss);
				// tm.tm_msec = (mmss-tm.tm_sec)*1000;
			}
			goto parse_timezone;
		}
		break;

		// hhmm<*>
		// hhmm<*>,m
	case 4:
		tm.tm_hour = value/100;
		tm.tm_min = (value%100);
		switch (*p) {
			// hhmm<*>
		default:
			goto parse_timezone;

		case '.': case ',':
			goto parse_min_frac;
		}
		break;

		// hhmmss<*>
		// hhmmss<*>,s
	case 6:
		tm.tm_hour = value/10000;
		tm.tm_min = (value/100)%100;
		tm.tm_sec = (value%100);
		switch (*p) {
			// hhmmss<*>
		default:
			goto parse_timezone;

			// hhmmss<*>,s
		case '.': case ',':
			goto parse_sec_frac;
		}
		break;
	}

	// getting to here is a failure
	return p;

#pragma mark State 4

// we have parsed the hours already
// hh:<*>mm:ss[,s]
// hh:<*>mm[,m]
parse_mm:
	switch (slurp(&p, &value)) {

		// hh:mm<*>:ss[,s]
		// hh:mm<*>[,m]
	case 2:
		tm.tm_min = value;
		switch (*p) {
		default:
			goto parse_timezone;

		// hh:mm<*>:ss[,s]
		case ':':
			p++;
			goto parse_ss;

		// hh:mm<*>[,m]
		case '.': case ',':
			goto parse_min_frac;
		}
	}
	return p;

#pragma mark State 5

// we have parsed the hours and minutes already
// hh:mm:<*>ss[,s]
parse_ss:
	switch (slurp(&p, &value)) {

		// hh:mm:ss<*>[,s]
	case 2:
		tm.tm_sec = value;
		switch (*p) {

		// hh:mm:ss<*>
		default:
			goto parse_timezone;

		// hh:mm:ss,<*>s
		case '.': case ',':
			goto parse_sec_frac;
		}
	}
	return p;

#pragma mark State 6

// hhmm<*>,m
// hh:mm<*>,m
parse_min_frac:
	frac = slurpf(&p);
	if (frac) {
		// 1M = 60S
		double ss = frac*60;
		tm.tm_sec = INT(ss);
		// tm.tm_msec = (ss-tm.tm_sec)*1000;
	}
	goto parse_timezone;


#pragma mark State 7

// hhmmss<*>,s
// hh:mm:ss,<*>s
parse_sec_frac:
	// fractional seconds - eew.
	frac = slurpf(&p);
	if (frac) {
		// we don't have anywhere to put fractional
		// seconds - struct tm doesn't do that stuff
		// so we parse it and forget it
		// tm.tm_msec = frac*1000;
	}
	goto parse_timezone;

// date and time have been parsed. look at the time zone specifier

#pragma mark State 8

parse_timezone:

	switch (*p) {
		// <datetime><*>
		// if we can't see a modifier we like, then this time must
		// be "localtime" in which case we can convert using mktime()
	default:
		*result = mktime(&tm);
		return 0;
	
		// <datetime><*>Z
		// Z means "relative to Greenwich" so we can convert
		// now and return
	case 'Z':
		*result = (timegm(&tm));
		return 0;

		// <datetime><*>+hh
		// <datetime><*>-hh
		// <datetime><*>+hhmm
		// <datetime><*>-hhmm
		// <datetime><*>+hh:mm
		// <datetime><*>-hh:mm
		// +/- mean here is a time with an offset from
		// greenwich - we need to parse the offset, then
		// compute what that time means
	case '+': case '-':
		q = p++;						// keep pointer to the sign.
		switch (slurp(&p,&value)) {
			// anything we don't expect, we return a pointer
			// to the failing character
		default:
			return p;

			// <datetime>+hh<*>
			// <datetime>-hh<*>
			// <datetime>+hh<*>:mm
			// <datetime>-hh<*>:mm
		case 2:
			tmoff = value*3600;
			if (*p == ':') {
				p++;
				// <datetime>+hh:<*>mm
				// <datetime>-hh:<*>mm
				if (slurp(&p,&value) == 2) {
					tmoff += value;
				} else {
					// TODO - wind back and fail
				}
			} else {
				// <datetime>+hh<*>
				// <datetime>-hh<*>
			}
			break;

			// <datetime>+<*>hhmm
			// <datetime>-<*>hhmm
		case 4:
			tmoff = INT(value/100)*3600 + (value%100);
			break;
		}
		if (*q == '-') tmoff = -tmoff;
		break;
	}

	// timegm() will clean up tm_wday and tm_yday, will zero tm_gmtoff
	// and set tm_zone="UTC"
	*result = (timegm(&tm));
	*result -= tmoff;
	return 0;
}
