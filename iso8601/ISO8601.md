#ISO8601

Executive Summary: This is clearly a standard written by a committee, none of whom wanted to compromise so they ended up dumping the entire kitchen sink in.

This module provides a parser that tries to meet the needs of someone trying to cope with most of the insane variants that ISO8601 defines.

##History

Because the actual standards are copyrighted documents, I can't provide links to them.  What follows is my executive summary, which should not be taken as a definitive version of the standard, but a clarification.

There seems to have been three revisions over the years, as far as I can tell.  

* ISO 8601:1988 (E)		First Edition.		1988-06-15.
* ISO 8601:2000 (E)		Second Edition.		2000-12-15.
* ISO 8601:2004 (E)		Third Edition.		2004-12-01.

##Scope

This document is concerned only with date formats defined by the latest (3rd) edition, and ignores:

* timespans
* subtleties involving leap-seconds
* truncated representations

Most of the standard documents use 12th April, 1985 as an example date and this summary does the same.  However, to make sense it is important to read this document as though it were 1999 (so that years in the current century begin with '19' - updating it for the 21st century would make many of the examples invalid)

When discussing year encoding the standard refers to extensions that can be agreed by the partners in the information exchange.  Some versions of the standard suggest extending years by 1 digit, others by 2.  This document assumes that 0000 to 9999 is a wide-enough range that extensions are not required.

##Date Encoding

It is not valid to use alternate seperator characters.  If seperaters are used for dates, they must be '-'.

###Calendar Date

Year values are always encoded as either 2 or 4 digits, with leading zeroes as required.
Month values are always encoded as 2 digits where 01 represents January and 12 represents December. Days of the month are always encoded as 2 digits between 01 and the corresponding month length.

Encoding       |Example         |Comments
:--------------|:---------------|:---------------------------
yy-mm-dd       |85-04-12        |April 12th, 85th year of current century
yymmdd         |850412          |April 12th, 85th year of current century
ccyy-mm        |1985-04         |Day not specified, April 1985
ccyy           |1985            |Month not specified, 1985
cc             |19              |Year within century not specified, 1900[^1]


ISO 8601:2000 included several "truncated" representations which involved the omission of leading values; these truncated representations are no longer a part of ISO8601:2004

Encoding       |Example         |Comments
:--------------|:---------------|:---------------------------
-yymm          |-8504           |April, in 85th year of current century
-yy-mm         |-85-04          |April, in 85th year of current century
-yy            |-85             |85th year of current century
--mmdd         |--0405          |5th April in current year
--mm-dd        |--04-05         |5th April in current year
--mm           |--04            |April in current year
---dd          |---05           |5th of current month

###Ordinal Date

Days of the year are always encoded as 3 digits between 001 and 366, and count from January 1st.

Encoding       |Example         |Comments
:--------------|:---------------|:---------------------------
ccyy-ddd       |1985-102        |Day#102 within 1985
ccyyddd        |1985102         |Day#102 within 1985
yy-ddd         |85-102          |Day#102 within 85th year of current century
yyddd          |85102           |Day#102 within 85th year of current century

ISO 8601:2000 included several "truncated" representations which involved the omission of leading values; these truncated representations are no longer a part of ISO8601:2004

Encoding       |Example         |Comments
:--------------|:---------------|:---------------------------
-ddd           |-102            |Day#102 within the current year

###Week + Day Number

Week numbers (within a year) are always encoded as 2 digits with values between 01 and 53. Days of the week are always encoded as 1 digit between 1 and 7, where 1 represents Monday and 7 represents Sunday. The first week in a calendar year is the week that contains January 4th.  This has the surprising result that sometimes the date of the Monday in the first week of a calendar year is actually in the previous year.  For example the first week of 2008 starts on Monday 31st December, 2007.  Similarly, Tuesday December 31, 1996 is the 2nd day of the 1st week of 1997.  The opposite is also possible: Sunday January 1, 1995 is actually the 7th day of the 52nd week of 1994.

Encoding       |Example         |Comments
:--------------|:---------------|:---------------------------
ccyy-Www-d     |1985-W15-5      |Day#5 within 15th week of 1985
ccyyWwwd       |1985W155        |Day#5 within 15th week of 1985
ccyy-Www       |1985-W15        |15th week of 1985
ccyyWww        |1985W15         |15th week of 1985
yy-Www-d       |1985-W15-5      |Day#5 within 15th week of 85th year of current century
yyWwwd         |1985W155        |Day#5 within 15th week of 85th year of current century
yy-Www         |1985-W15        |15th week of 85th year of current century
yyWww          |1985W15         |15th week of 85th year of current century

The 'W' indicator may also be a lower-case 'w'.

ISO 8601:2000 included several "truncated" representations which involved the omission of leading values; these truncated representations are no longer a part of ISO8601:2004

Encoding       |Example         |Comments
:--------------|:---------------|:---------------------------
-yWwwd         |-5W155          |5th day of 15th week of 5th year of current decade
-y-Www-d       |-5-W15-5        |5th day of 15th week of 5th year of current decade
-Wwwd          |-W155           |5th day of 15th week of current year
-Www-d         |-W15-5          |5th day of 15th week of current year
-Www           |-W15            |15th week of current year
-W-d           |-W-5            |5th day of current week
---d           |---5            |5th day of current week

##Time Encoding

Hours are represented by two digits from [00] to [24], minutes are represented by two digits from [00] to [59], and seconds are represented by two digits from [00] to [60]. For most purposes, times will be represented by four digits [hhmm].The representation of the hour by [24] is only allowed to indicate midnight (i.e. 24:00:00 is legal whereas 24:00:01 is not).[^2]
The representation of the second by [60] is only allowed to indicate the positive leap second or a time-point within that second.
It is not valid to use alternate seperator characters.  If seperaters are used for times, they must be ':'

Encoding       |Example         |          Comments
:--------------|:---------------|:---------------------------
hhmmss         |232050          |20 minutes and 50 seconds past 11pm
hh:mm:ss       |23:20:50        |20 minutes and 50 seconds past 11pm
hhmm           |2320            |20 minutes past 11pm
hh:mm          |23:20           |20 minutes past 11pm
hh             |23              |11pm

Fractional encodings are also permitted.  Note that while this summary shows a full stop (.) as a decimal seperator, the use of comma (,) is preferred by the standard (presumably due to its European origins).

Encoding       |Example         |          Comments
:--------------|:---------------|:---------------------------
hhmmss.s       |232050.5        |20 minutes, 50 and 5/10th seconds past 11pm
hh:mm:ss.s     |23:20:50.5      |20 minutes, 50 and 5/10th seconds past 11pm
hhmm.m         |2320.9          |20 and 9/10th minutes past 11pm
hh:mm.m        |23:20.9         |20 and 9/10th minutes past 11pm
hh.h           |23.3            |3/10ths of an hour past 11pm

The fractional component must have at least one digit following the decimal seperator, but may have as many as necessary.

ISO 8601:2000 included several "truncated" representations which involved the omission of leading values; these truncated representations are no longer a part of ISO8601:2004.  Interestingly, ISO8601:1988 listed 'hh.h' as both basic and truncated.  This seems to be an error in the spec.

Encoding       |Example         |           Comments
:--------------|:---------------|:---------------------------
-mmss          |-2050           |20 minutes, 50 seconds past current hour
-mm:ss         |-20:50          |20 minutes, 50 seconds past current hour
-mmss.s        |-2050.5         |20 minutes, 50 and 5/10th seconds past current hour
-mm:ss.s       |-2050.5         |20 minutes, 50 and 5/10th seconds past current hour
-mm            |-20             |20 minutes past current hour
-mm.m          |-20.9           |20 and 9/10ths minutes past current hour
--ss           |--50            |50 second past current minute
--ss.s         |--50.5          |50 and 5/10th seconds past current minute

## Time Zones

If the time is suffixed with a "Z", it is assumed to be expressed in UTC (Coordinated Universal Time).

In the absence of any trailing specifier, a time is assumed to be expressed in "local" time.

Encoding     |Example         |          Comments
:------------|:---------------|:---------------------------
time         |152746          |27 minutes, 46 seconds past 3pm, local time
timeZ        |152746Z         |27 minutes, 46 seconds past 3pm, UTC
time+hhmm    |152746+0100     |27 minutes, 46 seconds past 3pm, Geneva (UTC + 1 hour)
time+hh:mm   |152746+01:00    |27 minutes, 46 seconds past 3pm, Geneva (UTC + 1 hour)
time+hh      |152746+01       |27 minutes, 46 seconds past 3pm, Geneva (UTC + 1 hour)
time-hhmm    |152746-0500     |27 minutes, 46 seconds past 3pm, New York (UTC - 5 hour)
time-hh:mm   |152746-05:00    |27 minutes, 46 seconds past 3pm, New York (UTC - 5 hour)
time-hh      |152746-05       |27 minutes, 46 seconds past 3pm, New York (UTC - 5 hour)

## Combining date and time

For a complete specification of date and time, the two encoded values should be seperated by "T" although the spec allows for "mutual agreement by partners in information exchange" to omit this character if there is no ambiguity.

It is not valid to combine a date encoded with seperators (-) with a time encoded without seperators (:) or vice versa.

It is not valid to combine a time with a "reduced precision" date.  For example, 85-05T23:50 cannot be used to represent 50 minutes past 11pm of "an unspecified day in April 1985".  Reduced precision dates formats are as follows:

* ccyy-mm
* ccyy
* cc
* ccyy-Www 
* ccyyWww
* yy-Www
* yyWww

##Midnight

Midnight is encoded as 00:00:00 and is assumed to be the time immediately preceding the first second of the day.
Midnight may also be encoded as 24:00:00 and is assumed to be the immediately following the last second of the day - in effect, it represents the 00:00:00 of "tomorrow".  ie, 24:00:00 on 12th April is the same point in time as 00:00:00 on 13th April.


[^1]: Note the two-day encoding of a century is counter-intuitive but has been consistent across all three editions of the specification.  A two-digit number in isolation represents a century, not a year within a century.  "85" means the year 8500.

[^2]: See ISO8601:2004, 4.2.3
