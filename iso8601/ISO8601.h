//
//  ISO8601.h
//  xJSON
//
//  Created by Jeffrey Laing on 10/09/12.
//  Copyright (c) 2012 Jeffrey Laing. All rights reserved.
//
#pragma once

#include <time.h>

/// Parse a ISO8601-encoded date and return a time_t which represents the
/// time in seconds since the epoch.
/// Returns NULL on success, points to offending character on parse failure
/// <PRE>
/// time_t sometime;
/// if (Parse8601(str, &sometime)) {
///		date = [NSDate dateWithTimeIntervalSince1970:sometime]);
/// }
/// </PRE>
const char *Parse8601(
	const char *utf8,
	time_t *result );
