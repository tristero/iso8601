//
//  iso8601_tests.m
//  iso8601.tests
//
//  Created by Jeff Laing on 5/12/16.
//  Copyright © 2016 Jeff Laing. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ISO8601.h"

@interface iso8601_tests : XCTestCase

@end

@implementation iso8601_tests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

    /*
     
     2000:5.4.2
     Representations other than complete
     For reduced precision, decimal, truncated or expanded representations of a combined date and time of the day expression any of the representations in 5.2.1 (for calendar dates), 5.2.2 (for ordinal dates) or 5.2.3 (for week dates) followed immediately by the time designator [T], may be combined with any of the representations in 5.3.1.1 through 5.3.1.3 (local time of the day), 5.3.3 (UTC) or 5.3.4.2 (local time and the difference with UTC) provided that:
     a) the rules specified in those sections are applied;
     b) the resulting expression does not qualify as a complete representation in accordance with 5.4.1;
     c) the date component shall not be represented with reduced precision and the time component shall not be truncated. Note that this excludes the date representations in 5.2.1.3 and 5.2.3.3 that are truncated and reduced and the date representations in 5.2.1.4 and 5.2.3.4 that are expanded and reduced;
     d) the expression shall either be completely in basic format, in which case the minimum number of separators necessary for the required expression is used, or completely in extended format, in which case additional separators shall be used in accordance with 5.2 and 5.3.
     
     2004:4.3.3
     For reduced accuracy, decimal or expanded representations of date and time of day, any of the representations in 4.1.2 (calendar dates), 4.1.3 (ordinal dates) or 4.1.4 (week dates) followed immediately by the time designator [T] may be combined with any of the representations in 4.2.2.2 through 4.2.2.4 (local time), 4.2.4 (UTC of day) or 4.2.5.2 (local time and the difference from UTC) provided that
     a) the rules specified in those sections are applied;
     b) the resulting expression does not qualify as a complete representation in accordance with 4.3.2;
     c) the date component shall not be represented with reduced accuracy;
     NOTE This excludes also the date representations in 4.1.2.4 and 4.1.4.4 that are expanded and reduced.
     d) the expression shall either be completely in basic format, in which case the minimum number of separators necessary for the required expression is used, or completely in extended format, in which case additional separators shall be used in accordance with 4.1 and 4.2.
     */
    
#if 0

(circa 2000)
5.2.1.3 Truncated representations
If, by agreement, truncated representations are used the basic formats shall be as specified below. In each case hyphens that indicate omitted components shall be used only as indicated or shall be omitted.
a) A specific date in the implied century Basic format: YYMMDD Extended format: YY-MM-DD
b) A specific year and month in the implied century Basic format: -YYMM
Extended format: -YY-MM
c) A specific year in the implied century Basic format: -YY
Extended format: not applicable
d) A specific day of a month in the implied year Basic format: --MMDD
Extended format: --MM-DD
e) A specific month in the implied year Basic format: --MM
Extended format: not applicable
f) A specific day in the implied month Basic format: ---DD
Extended format: not applicable

5.2.2.2 Truncated representations
If, by agreement, truncated representations are used, the basic formats shall be as specified below. In each case hyphens that indicate omitted components shall be used only as indicated or shall be omitted.
a) A specific year and day in the implied century Basic format: YYDDD
Extended format: YY-DDD
b) Day only in the implied year Basic format: -DDD Extended format: not applicable
NOTE Logically, the representation should be [--DDD], but the first hyphen is superfluous and, therefore, it has been omitted.


a) Year, week and day in the implied century Basic format: YYWwwD
Extended format: YY-Www-D
b) Year and week only in the implied century Basic format: YYWww
Extended format: YY-Www
c) Year of the implied decade, week and day only Basic format: -YWwwD
Extended format: -Y-Www-D
d) Year of the implied decade and week only Basic format: -YWww
Extended format: -Y-Www
e) Week and day only of the implied year Basic format: -WwwD
Extended format: -Www-D
f) Week only of the implied year Basic format: -Www Extended format: not applicable
g) Day only of the implied week Basic format: -W-D Extended format: not applicable
EXAMPLE 85W155 EXAMPLE 85-W15-5
EXAMPLE 85W15 EXAMPLE 85-W15
EXAMPLE -5W155 EXAMPLE -5-W15-5
EXAMPLE -5W15 EXAMPLE -5-W15
EXAMPLE -W155 EXAMPLE -W15-5
EXAMPLE -W15
EXAMPLE -W-5
NOTE 1
NOTE 2 omitted.
5.2.3.3 includes the definition of representations which are both truncated and have reduced precision.
Logically the representations should be [--W-D], but the first hyphen is superfluous and, therefore, it has been

5.3.1.4 Truncated representations
If, by agreement, truncated representations are used the basic formats shall be as specified below. In each case hyphens that indicate omitted components, shall be used only as indicated or shall be omitted.
a) A specific minute and second of the implied hour
Basic format: -mmss Extended format: -mm:ss
b) A specific minute of the implied hour Basic format: -mm
Extended format: not applicable
c) A specific second of the implied minute Basic format: --ss
Extended format: not applicable
EXAMPLE -2050 EXAMPLE -20:50
EXAMPLE -20
EXAMPLE --50
d) A specific minute and second of the implied hour and a decimal fraction of the second Basic format: -mmss,s EXAMPLE -2050,5
Extended format: -mm:ss,s EXAMPLE -20:50,5
e) A specific minute of the implied hour and a decimal fraction of the minute
Basic format: -mm,m EXAMPLE -20,8 Extended format: not applicable
f) A specific second of the implied minute and a decimal fraction of the second Basic format: --ss,s EXAMPLE --50,5 Extended format: not applicable
NOTE 1 In the examples it has been agreed to expand the smallest time element with one digit.
NOTE 2 5.3.1.4 includes the definition of representations that have been truncated and have reduced precision and/or a decimal fraction.

For reduced precision, decimal, truncated or expanded representations of a combined date and time of the day expression any of the representations in 5.2.1 (for calendar dates), 5.2.2 (for ordinal dates) or 5.2.3 (for week dates) followed immediately by the time designator [T], may be combined with any of the representations in 5.3.1.1 through 5.3.1.3 (local time of the day), 5.3.3 (UTC) or 5.3.4.2 (local time and the difference with UTC) provided that:
a) the rules specified in those sections are applied;
b) the resulting expression does not qualify as a complete representation in accordance with 5.4.1;
c) the date component shall not be represented with reduced precision and the time component shall not be truncated. Note that this excludes the date representations in 5.2.1.3 and 5.2.3.3 that are truncated and reduced and the date representations in 5.2.1.4 and 5.2.3.4 that are expanded and reduced;
d) the expression shall either be completely in basic format, in which case the minimum number of separators necessary for the required expression is used, or completely in extended format, in which case additional separators shall be used in accordance with 5.2 and 5.3.


https://gist.github.com/2312309

'2012-04-05T15:53:00Z'
//Firefox 11/Chrome 18  Safari 5.1.1     Opera 12/IE 9
'2012-04-05T15:53:00.Z'
'2012-04-05T15:53:00.1Z'
'2012-04-05T15:53:00.12Z'
'2012-04-05T15:53:00.123Z'
'2012-04-05T15:53:00.1234Z'
'2012-04-05T15:53:00.1235Z'
'2012-04-05T15:53:00.1239Z'
'2012-04-05T15:53:00.1234567890Z'
'2012-04-05T15:53:00.12345678900Z'

//Chrome 19     Opera 12       Firefox 11    IE 9          Safari 5.1.1
"2012-11-31T23:59:59.000Z"
"2012-12-31T23:59:59.000Z"
"2012-12-31T23:59:60.000Z"
"2012-04-04T05:02:02.170Z"
"2012-04-04T24:00:00.000Z"
"2012-04-04T24:00:00.500Z"
"2012-12-31T10:08:60.000Z"
"2012-13-01T12:00:00.000Z"
"2012-12-32T12:00:00.000Z"
"2012-12-31T25:00:00.000Z"
"2012-12-31T24:01:00.000Z"
"2012-12-31T12:60:00.000Z"
"2012-12-31T12:00:60.000Z"
"2012-00-31T23:59:59.000Z"
"2012-12-00T23:59:59.000Z"
"2012-02-29T12:00:00.000Z"
"2011-02-29T12:00:00.000Z"
"2011-03-01T12:00:00.000Z"

// extended years:
"0000-01-01T00:00:00.000Z"
"+275760-09-13T00:00:00.000Z"
"-271821-04-20T00:00:00.000Z"
"+275760-09-13T00:00:00.001Z"
"-271821-04-19T23:59:59.999Z"

// https://github.com/kriskowal/es5-shim/issues/80 Safari bug with leap day
"2034-03-01T00:00:00.000Z"
"2034-02-27T23:59:59.999Z"

// Time Zone Offset
"2012-01-29T12:00:00.000+01:00"
"2012-01-29T12:00:00.000-00:00"
"2012-01-29T12:00:00.000+00:00"
"2012-01-29T12:00:00.000+23:59"
"2012-01-29T12:00:00.000-23:59"
"2012-01-29T12:00:00.000+24:00"
"2012-01-29T12:00:00.000+24:01"
"2012-01-29T12:00:00.000+24:59"
"2012-01-29T12:00:00.000+25:00"
"2012-01-29T12:00:00.000+00:60"
"-271821-04-20T00:00:00.000+00:01"
"-271821-04-20T00:01:00.000+00:01"

"1970-01-01T00:00:00"

"19700101"
"2012-04-12T17:00:00-04:00"
"2006-09-12T20:33:11Z"
"20060912T20:33:11Z"
"1994-11-05T08:15:30-05:00"
"1994-11-05T13:15:30Z"

// another summary of ISO8601 circa 1998
"19930214" or "1993-02-14" (complete representation)
"1993-02" (reduced precision)
"1993"
"19"
"930214" or "93-02-14" (truncated, current century assumed)
"-9302" or "-93-02"
"-93"
"--0214" or "--02-14"
"--02"
"---14"
"1993045" or "1993-045" (complete representation)
"93045" or "93-045"
"-045"
"1993W067" or "1993-W06-7" (complete representation)
"1993W06" or "1993-W06"
"93W067" or "93-W06-7"
"93W06" or "93-W06"
"-3W067" or "-3-W06-7"
"-W067" or "-W06-7"
"-W06"
"-W-7" (day of current week)
"---7" (day of any week)

"131030" or "13:10:30" (complete representation)
"1310" or "13:10"
"13"
"-1030" or "-10:30"
"-10"
"--30"

Peter Hosey
• If the month or month and date are missing, 1 is assumed. ["2006" == "2006-01-01".
• If the year or year and month are missing, the current ones are assumed. "--02-01" == "2006-02-01". "---28" == "2006-02-28".
• In the case of week-based dates, with  the day missing, this implementation returns the first day of that week: 2006-W1 is 2006-01-01, 2006-W2 is 2006-01-08, etc.
• For any date without a time, midnight on that date is used.
• ISO 8601 permits the choice of either T0 or T24 for midnight. This implementation uses T0. T24 will get you T0 on the following day.
• If no time-zone is specified, local time (as returned by [NSTimeZone localTimeZone]) is used.

When a date is parsed that has a year but no century, this implementation adds the current century.

The implementation is tolerant of out-of-range numbers. For example, "2005-13-40T24:62:89" == 1:02 AM on 2006-02-10. Notice that the month (13 > 12), date (40 > 31), hour (24 > 23), minute (62 > 59), and second (89 > 59) are all out-of-range.

#endif
#if 0
void testdates()
{
    NSLog(@"Testing dates");
    const char **d;
    
    NSLog(@"Now=%@",[NSDate dateWithTimeIntervalSinceNow:0]);
    d = gooddates;
    while (*d) {
        time_t t = 0;
        const char *fail;
        if (**d != '+' && **d != '-') {
            fail = Parse8601(*d, &t);
            if (fail) {
                NSLog(@"%30s = Failed at %.*s<*>%s", *d, (int)(fail-*d), *d, fail);
            } else {
                NSLog(@"%30s = %@ %s", *d, [NSDate dateWithTimeIntervalSince1970:t], ctime(&t));
            }
        }
        d++;
    }
    
    d = baddates;
    while (*d) {
        time_t t = 0;
        const char *fail;
        if (**d != '+' && **d != '-') {
            fail = Parse8601(*d, &t);
            if (fail) {
                NSLog(@"%30s = Failed correctly at %.*s<*>%s", *d, (int)(fail-*d), *d, fail);
            } else {
                NSLog(@"%30s = Oops, looks like %@ %s", *d, [NSDate dateWithTimeIntervalSince1970:t], ctime(&t));
            }
        }
        d++;
    }
}
#endif
                                                             
- (void)testExample {
    time_t t;
    const char *fail;
    
#define GOOD(s) do { \
        if (NULL != (fail = Parse8601(s, &t))) \
            _XCTPrimitiveFail(self, @"Failed at %.*s<*>%s", (int)(fail-s), s, fail); \
    } while(0)

#define BAD(s) do { \
    if (NULL == (fail = Parse8601(s, &t))) \
        _XCTPrimitiveFail(self, @"Parsed as %@", [NSDate dateWithTimeIntervalSince1970:t]); \
} while(0)

    GOOD("T15:27:46");
    GOOD("T15:27:46Z");

    // 2000:5.2.1.1, 2004:4.1.2.2
    GOOD("19850412");
    GOOD("1985-04-12");

    // 2000:5.2.1.2, 2004:4.1.2.3 (reduced precision)
    GOOD("1985-04");			// a specific month
    GOOD("1985");				// a specific year
    GOOD("19");                 // a specific century

    // 2000:5.2.1.3, 2004:omitted (truncated)
    GOOD("850412");			// implied century
    GOOD("85-04-12");

    BAD("-8504");			// month in year in implied century
    BAD("-85-04");			//
    BAD("-85");
    BAD("--0412");			// day of month in implied year
    BAD("--04-12");
    BAD("--04");			// month in year
    BAD("---12");			// day in implied month

    // 2000:5.2.1.4, 2004:4.1.2.4 (expanded)
    BAD("+0019850412");
    BAD("+001985-04-12");
    BAD("+001985-04");
    BAD("+001985-04");
    BAD("+001985");
    BAD("+0019");

    // 2000:5.2.2.1, 2004:4.1.3.2 (Ordinal Dates)
    GOOD("1985102");
    GOOD("1985-102");

    // 2000:5.2.2.2, 2004:omitted (truncated)
    GOOD("85102");
    GOOD("85-102");
    BAD("-102");


    // 2000:5.2.2.3, 2004:4.1.3.3 (expanded)
    BAD("+001985102");
    BAD("+001985-102");

    // 2000:5.2.2.3, 2004:omitted
    BAD("+001985-04");			// a specific month
    BAD("+001985");				// a specific year
    BAD("+0019");				// a specific century

    // 2000:5.2.3.1, 2004:4.1.4.2
    GOOD("1985W155");
    GOOD("1985-W15-5");

    // 2000:5.2.3.2, 2004:4.1.4.3 (reduced precision)
    GOOD("1985W15");
    GOOD("1985-W15");

    GOOD("2008-W01-1");			// actually 2007-12-31
    GOOD("2009-W01-1");			// actually 2008-12-29

    // 2000:5.2.3.3, 2004:omitted (truncated)
    GOOD("85W155");
    GOOD("85-W15-5");
    GOOD("85W15");
    GOOD("85-W15");
    BAD("-5W155");
    BAD("-5-W15-5");
    BAD("-W155");
    BAD("-W15-5");

    // 2000:5.2.3.3, 2004:omitted (reduced precision + truncated)
    BAD("-W15");					// week only
    BAD("-W-5");				// day 5 of implied week

    // 2000:5.2.3.4, 2004:4.1.4.4 (expanded)
    BAD("+001985W155");
    BAD("+001985-W15-5");
    BAD("+001985W15");
    BAD("+001985-W15");

    // 2000:5.3.1.1, 2004:4.2.2.2
    GOOD("T232050");				// hhmmss
    GOOD("T23:20:50");

    // 2000:5.3.1.2, 2004:4.2.2.3 (reduced precision)
    GOOD("T2320");				// hhmm
    GOOD("T23:20");
    GOOD("T23");					// hh

    // 2000:5.3.1.3, 2004:4.2.2.4 decimal (, or .)
    GOOD("T232050,5");			// hhmmss.s
    GOOD("T23:20:50,5");
    GOOD("T2320,8");				// hhmm.m
    GOOD("T23:20,8");
    GOOD("T23,3");				// hh.h

    // 2000:5.3.1.4, 2004:omitted (truncated)
    BAD("T-2050");				// mmss
    BAD("T-20:50");
    BAD("T-20");					// mm
    BAD("T--50");				// ss

    // 2000:5.3.1.4, 2004:omitted (truncated + decimal)
    BAD("T-2050,5");				// hhmm.m
    BAD("T-20:50,5");
    BAD("T-20,8");				// mm.m
    BAD("T--50,5");				// ss.s

    // 2000:5.3.2, 2004:4.2.3
    GOOD("T000000");
    GOOD("T240000");				// equivalent to T000000 on day+1

    // 2000:5.3.3, 2004:4.2.4
    // "" means localtime
    // "Z" means UTC
    GOOD("T232030");

    GOOD("T232030Z");
    GOOD("T2320Z");
    GOOD("T23Z");
    GOOD("T23:20:30Z");
    GOOD("T23:20Z");

    GOOD("T152746Z");
    GOOD("T152746+0100");
    GOOD("T152746-0500");
    GOOD("T152746+01");
    GOOD("T152746-05");

    GOOD("T15:27:46Z");
    GOOD("T15:27:46+01:00");
    GOOD("T15:27:46-05:00");
    GOOD("T15:27:46+01");
    GOOD("T15:27:46-05");

    // examples 2000:x.x.x.x, 2004:4.3.2
    GOOD("19850412T101530");
    GOOD("19850412T101530Z");
    GOOD("19850412T101530+0400");
    GOOD("19850412T101530+04");

    GOOD("1985-04-12T10:15:30");
    GOOD("1985-04-12T10:15:30Z");
    GOOD("1985-04-12T10:15:30+0400");
    GOOD("1985-04-12T10:15:30+04");

    GOOD("2009-12T12:34");
    GOOD("2009");
    GOOD("2009-05-19");
    GOOD("20090519");
    GOOD("2009123");
    GOOD("2009-05");
    GOOD("2009-123");
    GOOD("2009-222");
    GOOD("2009-001");
    GOOD("2009-W01-1");
    GOOD("2009-W51-1");
    GOOD("2009-W511");
    GOOD("2009-W33");
    GOOD("2009W511");
    GOOD("2009-05-19");
    BAD("2009-05-19 00:00");				// alternate delimiter?
    BAD("2009-05-19 14");					// alternate delimiter?
    BAD("2009-05-19 14:31");				// alternate delimiter?
    BAD("2009-05-19 14:39:22");             // alternate delimiter?
    GOOD("2009-05-19T14:39Z");
    GOOD("2009-W21-2");
    GOOD("2009-W21-2T01:22");
    GOOD("2009-139");
    BAD("2009-05-19 14:39:22-06:00");		// alternate delimiter?
    BAD("2009-05-19 14:39:22+0600");		// alternate delimiter?
    BAD("2009-05-19 14:39:22-01");			// alternate delimiter?
    GOOD("20090621T0545Z");
    GOOD("2007-04-06T00:00");
    GOOD("2007-04-05T24:00");
    GOOD("2010-02-18T16:23:48.5");
    GOOD("2010-02-18T16:23:48,444");
    GOOD("2010-02-18T16:23:48,3-06:00");
    GOOD("2010-02-18T16:23.4");
    GOOD("2010-02-18T16:23,25");
    GOOD("2010-02-18T16:23.33+0600");
    GOOD("2010-02-18T16.23334444");
    GOOD("2010-02-18T16,2283");
    BAD("2009-05-19 143922.500");			// alternate delimiter?
    BAD("2009-05-19 1439,55");				// alternate delimiter?
    GOOD("12");
    GOOD("1985");
    GOOD("0000");
    GOOD("2400");
    GOOD("19850412");
    GOOD("1985102");
    GOOD("1985W155");
    GOOD("1985W15");
    GOOD("1985-04");
    GOOD("T152746");
    GOOD("T1528");
    GOOD("T15");

    GOOD("19850412T101530");
    GOOD("1985102T235030Z");
         
     GOOD("19850412T1015");
     GOOD("1985-04-12T10:15");
     GOOD("1985102T1015Z");
     GOOD("1985-102T10:15Z");
     GOOD("1985W155T1015+0400");
     GOOD("1985-W15-5T10:15+04");
    
#if 0
     GOOD("152735,5");
     GOOD("000000");
     GOOD("240000");
     GOOD("232030Z");
     GOOD("2320Z");
     GOOD("23Z");
     GOOD("152746+0100");
     GOOD("152746+01");
     GOOD("152746-0500");
     GOOD("152746-05");
#endif
     GOOD("1985W155T235030");

    BAD("200905");
    BAD("2009367");
    BAD("2009-");
    BAD("2007-04-05T24:50");
    BAD("2009-000");
    BAD("2009-M511");
    BAD("2009M511");
    BAD("2009-05-19T14a39r");
    BAD("2009-05-19T14:3924");
    BAD("2009-0519");
    BAD("2009-05-1914:39");
    BAD("2009-05-19 14:");
    BAD("2009-05-19r14:39");
    BAD("2009-05-19 14a39a22");
    BAD("200912-01");
    BAD("2009-05-19 14:39:22+06a00");
    BAD("2009-05-19 146922.500");
    BAD("2010-02-18T16.5:23.35:48");
    BAD("2010-02-18T16:23.35:48");
    BAD("2010-02-18T16:23.35:48.45");
    BAD("2009-05-19 14.5.44");
    BAD("2010-02-18T16:23.33.600");
    BAD("2010-02-18T16,25:23:48,444");

GOOD("19850412");
GOOD("1985102");
GOOD("1985W155");
GOOD("1985W15");
GOOD("1985-04");
GOOD("1985");
GOOD("T152746");
GOOD("T1528");
GOOD("T15");
GOOD("152735,5");
GOOD("000000");
GOOD("0000");
GOOD("240000");
GOOD("2400");
GOOD("232030Z");
GOOD("2320Z");
GOOD("23Z");
GOOD("152746+0100");
GOOD("152746+01");
GOOD("152746-0500");
GOOD("152746-05");
GOOD("19850412T101530");
GOOD("1985102T235030Z");
GOOD("1985W155T235030");

// http://stackoverflow.com/questions/10005374/ecmascript-5-date-parse-results-for-iso-8601-test-cases

GOOD("2012-04-03 23:50:00+10:00");
GOOD("2012-04-03 23:50:00-04:15");
GOOD("2012-04-03 23:50:00+10");
GOOD("20120403T235000+1000");

GOOD("2012-11-31T23:59:59.000Z");
GOOD("2012-12-31T23:59:59.000Z");
GOOD("2012-12-31T23:59:60.000Z");
GOOD("2012-04-04T05:02:02.170Z");
GOOD("2012-04-04T24:00:00.000Z");
GOOD("2012-04-04T24:00:00.500Z");
GOOD("2012-12-31T10:08:60.000Z");
GOOD("2012-13-01T12:00:00.000Z");
GOOD("2012-12-32T12:00:00.000Z");
GOOD("2012-12-31T25:00:00.000Z");
GOOD("2012-12-31T24:01:00.000Z");
GOOD("2012-12-31T12:60:00.000Z");
GOOD("2012-12-31T12:00:60.000Z");
GOOD("2012-00-31T23:59:59.000Z");
GOOD("2012-12-00T23:59:59.000Z");
GOOD("2012-02-29T12:00:00.000Z");
GOOD("2011-02-29T12:00:00.000Z");
GOOD("2011-03-01T12:00:00.000Z");

// extended years:
GOOD("0000-01-01T00:00:00.000Z");
GOOD("+275760-09-13T00:00:00.000Z");
GOOD("-271821-04-20T00:00:00.000Z");
GOOD("+275760-09-13T00:00:00.001Z");
GOOD("-271821-04-19T23:59:59.999Z");

GOOD("2034-03-01T00:00:00.000Z");
GOOD("2034-02-27T23:59:59.999Z");

// Time Zone Offset
GOOD("2012-01-29T12:00:00.000+01:00");
GOOD("2012-01-29T12:00:00.000-00:00");
GOOD("2012-01-29T12:00:00.000+00:00");
GOOD("2012-01-29T12:00:00.000+23:59");
GOOD("2012-01-29T12:00:00.000-23:59");
GOOD("2012-01-29T12:00:00.000+24:00");
GOOD("2012-01-29T12:00:00.000+24:01");
GOOD("2012-01-29T12:00:00.000+24:59");
GOOD("2012-01-29T12:00:00.000+25:00");
GOOD("2012-01-29T12:00:00.000+00:60");
GOOD("-271821-04-20T00:00:00.000+00:01");
GOOD("-271821-04-20T00:01:00.000+00:01");

}

@end
